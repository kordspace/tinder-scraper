import requests
import json
import sys
import urllib.request
import os, os.path
import random
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

'''
For this script to run, the following must be specified, and the user must create an "imagePath" folder for the photos to be saved to.
'''

facebook_token = ''	# Refer to README.md for how to get token
facebook_id = ''	# Refer to README.md for how to get facebook ID
imagePath = ''		# Name of folder images save to 


def waitABit(minTime, maxTime):
	wait = random.uniform(minTime, maxTime)
	print("WAIT: " + str(wait) + "\n")
	time.sleep(wait)


def tinderAPI_get_xAuthToken(facebook_token, facebook_id):

	loginCredentials = {'facebook_token': facebook_token, 'facebook_id': facebook_id}
	headers = {'Content-Type': 'application/json', 'User-Agent': 'Tinder Android Version 3.2.0'}

	r = requests.post('https://api.gotinder.com/auth', data=json.dumps(loginCredentials), headers=headers)
	x_auth_token = r.json()['token']

	return x_auth_token


def tinderAPI_getSubjectList(x_auth_token):

	# Get a list of subjects
	headers2 = {'User-Agent': 'Tinder Android Version 3.2.0', 'Content-Type': 'application/json', 'X-Auth-Token': x_auth_token}
	r2 = requests.get('https://api.gotinder.com/user/recs', headers=headers2)
	subjects = r2.json()['results']

	# Return list of subjects
	return subjects


def tinderAPI_passSubject(subject, x_auth_token):
	_id = subject['_id']
	headers3 = {'X-Auth-Token': x_auth_token, 'User-Agent': 'Tinder Android Version 3.2.0'}
	r3 = requests.get('https://api.gotinder.com/pass/' + _id, headers=headers3)


def getPics(x_auth_token):

	# Get list of subjects
	subjects = tinderAPI_getSubjectList(x_auth_token)

	# Get the number of photos in directory
	processed_numPhotos = len([f for f in os.listdir(imagePath) if os.path.isfile(os.path.join(imagePath, f))])

	# Iterate through list of subjects
	for subject in subjects:
		
		# Get the subject ID
		sid = subject['_id']
		
		# Gets a list of pictures of the subject
		pictures = subject['photos']

		# Iterate through and save the pictures of the subject
		for picIndex in range(len(pictures)):

			# Get the URL for the largest cropped photo
			processed_picURL = str(pictures[picIndex]['processedFiles'][0]['url'])

			# Get the photo and save
			urllib.request.urlretrieve(processed_picURL, imagePath + '/' + sid + '_' + str(processed_numPhotos) + '.jpg')
			processed_numPhotos += 1

		# Wait some random amount of time and then pass the subject
		waitABit(0.5, 2.0)
		
		# Pass the subject
		tinderAPI_passSubject(subject, x_auth_token)


if __name__ == '__main__':

	# Log into Tinder
	x_auth_token = tinderAPI_get_xAuthToken(facebook_token, facebook_id)

	# Get pics
	for i in range(10000):

		# Print current iteration to terminal
		print("Potential Match Batch: ",str(i + 1))

		# Get one collection of subjects and their pictures
		getPics(x_auth_token)

		# Wait a bit
		waitABit(0.25 * 60, 0.5 * 60)





________________________________

READ ME

This is a simple script that exploits the Tinder API to allow a person to build a facial dataset.

Inspiration

Having worked with facial datasets in the past, I have often been disappointed. The datasets tend to be extremely strict in their structure, and are usually too small. Tinder gives you access to thousands of people within miles of you. Why not leverage Tinder to build a better, larger facial dataset?

Running The Script

To run the script tinderGetPhotos.py, one must first do the following:

Download the Tinder app, create a profile, and get it up and running.
Create a folder to which the scraped photos will be saved. Then fill in the imagePath variable in the script.
Get your FaceBook ID from https://findmyfbid.in/. Then fill in the facebook_id variable in the script.
Get your FaceBook token by clicking here. Click Ok, and then look in the response to the POST request https://www.facebook.com/v2.6/dialog/oauth/confirm?dpr=1. One way to see the response to the POST request is to open Developer Tools in Google Chrome prior to clicking Ok, and navigating to the Network tab. In the response, one's token will be the string sandwiched between &access_token= and &expires_in=. Fill in the facebook_token variable in the script.
The Dataset

I have used this script to create a dataset of 20,000 male and female individuals. This dataset can be found here on Kaggle.

UPDATE: I have spoken with representatives at Kaggle, and they have received a request from Tinder to remove the dataset. As such, the facial data set previously hosted on Kaggle has been removed. A TechCrunch article about the dataset in question can be found here.

Future Work

I plan on using the aforementioned dataset with TensorFlow's Inception to try and create a CNN that is capable of distinguishing between men and women.





